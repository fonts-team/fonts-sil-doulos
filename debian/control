Source: fonts-sil-doulos
Section: fonts
Priority: optional
Maintainer: Debian Fonts Task Force <debian-fonts@lists.debian.org>
Uploaders: Nicolas Spalinger <nicolas.spalinger@sil.org>,
           Daniel Glassey <wdg@debian.org>,
           Bobby de Vos <bobby_devos@sil.org>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.2
Homepage: https://software.sil.org/doulos
Vcs-Git: https://salsa.debian.org/fonts-team/fonts-sil-doulos.git
Vcs-Browser: https://salsa.debian.org/fonts-team/fonts-sil-doulos
Rules-Requires-Root: no

Package: fonts-sil-doulos
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Replaces: fonts-sil-doulossil (<< 5.000-developer-2~)
Breaks: fonts-sil-doulossil (<< 5.000-developer-2~)
Description: Font family for wide range of languages that use the Latin and Cyrillic scripts
 Doulos is very similar to Times/Times New Roman, but only has a single
 face - regular. It is intended for use alongside other Times-like fonts where
 a range of styles (italic, bold) are not needed.
 .
 The goal for this product was to provide a single Unicode-based font family
 that would contain a comprehensive inventory of glyphs needed for almost any
 Roman- or Cyrillic-based writing system, whether used for phonetic or
 orthographic needs. In addition, there is provision for other characters and
 symbols useful to linguists. This font makes use of state-of-the-art font
 technologies to support complex typographic issues, such as the need to
 position arbitrary combinations of base glyphs and diacritics optimally.
 .
 One font from this typeface family is included in this release:
  * Doulos SIL Regular
 .
 Webfont versions and HTML/CSS examples are also available.
 .
 The full font sources are publicly available at
 https://github.com/silnrsi/font-doulos
 An open workflow is used for building, testing and releasing.
